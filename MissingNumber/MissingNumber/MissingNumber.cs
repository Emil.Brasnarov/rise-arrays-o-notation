﻿
namespace MissingNumber
{
    public class MissingNumber
    {
        public static void Main()
        {  
            Console.WriteLine("Enter the array elements separated by commas: ");
            string[] inputArray = Console.ReadLine().Split(',');

            int[] intArray = new int[inputArray.Length];

            for (int currentIndex = 0; currentIndex < intArray.Length; currentIndex++)
            {
                intArray[currentIndex] = int.Parse(inputArray[currentIndex]);
               // Console.WriteLine(intArray[currentIndex]);
            }

            var missing = FindMissing(intArray);

            Console.WriteLine($"The missing element is {missing}");
        }
   
        public static int FindMissing(int[] inputArray) { 
            int maxElement = int.MinValue; 
            int minElement = int.MaxValue;

            for (int currentIndex = 0; currentIndex < inputArray.Length; currentIndex++)
            {
                if (inputArray[currentIndex] > maxElement)
                {
                    maxElement = inputArray[currentIndex];
                }
                if (inputArray[currentIndex] < minElement)
                {
                    minElement = inputArray[currentIndex];
                }
            }

            int missingElement;
            int sumElementsActual = inputArray.Sum();
            int sumElementsExpected = 0;

            for (int currentElement = minElement; currentElement <= maxElement; currentElement++) {
                sumElementsExpected += currentElement;
            }
            missingElement = sumElementsExpected - sumElementsActual;

            return missingElement;
            
        }
    }
}