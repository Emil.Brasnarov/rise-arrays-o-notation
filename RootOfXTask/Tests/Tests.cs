using CubicRootX;
namespace Tests
{
    [TestClass]
    public class Tests
    {
        [TestMethod]
        public void TestCubicRootWithOneHundredTwentyFive()
        {
            string input = "125";
            int integerInput = Convert.ToInt32(input);
            int expected = 5;

            int actual = CubicRootX.CubicRootX.GetCubicRoot(integerInput);

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestCubicRootWithOne()
        {
            string input = "1";
            int integerInput = Convert.ToInt32(input);
            int expected = 1;

            int actual = CubicRootX.CubicRootX.GetCubicRoot(integerInput);

            Assert.AreEqual(expected, actual);
        }
    }
}