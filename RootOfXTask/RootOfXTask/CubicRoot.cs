﻿namespace CubicRootX
{
    public class CubicRootX
    {
        static void Main()
        {
            Console.WriteLine("Please enter an integer: ");

            string input = Console.ReadLine();
            int x = int.Parse(input);
            int result = GetCubicRoot(x);

            Console.WriteLine($"The cubic root value of x is {result}");
        }

        public static int GetCubicRoot(int x)
        {
            double rootResult = Math.Pow(x, (1.0/3.0));            
            return (int)rootResult;
        }
    }
    
}